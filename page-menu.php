<?php get_header(); ?>
	<div id='content'>


		<div id='main'>

			<div id='post'>

				<?php while ( have_posts() ) : the_post(); ?>
					<div class='title'>
						<?php the_title(); ?>
					</div>
<!--					<div class='text'>-->
<!--						--><?php //the_content(); ?>
<!--					</div>-->
				<?php endwhile; ?>
			
			</div> <!-- post -->

			<div id='menu-nav'>
				<script>
					$(document).ready(function() {

						//Get original doc position of menu nav top
						menuTop = $('#menu-buttons').offset().top;

						//Initialize flag to false
						booIsFixed = false;

						//Call function after page loads
						FixMenuNav();

						function FixMenuNav() {
							$menu = $('#menu-buttons');
							switch (IsFixMenu()) {
								case 1:
									console.log(1);
									//This case is when it is not fixed but should be
									$menu.css({
										'position':'fixed',
										'top':'0px',
										'left':'0px',
										'background-color':'#B8C92B',
										'width':'100%',
										'color':'white',
										'border-bottom':'1px solid white'
									});
									booIsFixed = true;

									break;
								case 2:
									//This case is when no action is necessary
									console.log(2);

									break;

								case 3:
									//This case is when it is fixed but should not be
									console.log(3);
									$menu.css({
										'position':'inherit',
										'top':'inherit',
										'left':'inherit',
										'background-color':'inherit',
										'width':'',
										'color':'#555',
										'border-bottom':'inherit'
									});
									booIsFixed = false;

									break;
							}
						}

						//Function to determine if menu nav should be affixed at top of page
						function IsFixMenu() {
							//Get scrolled amount of document
							docScroll = $(window).scrollTop();
							if(menuTop < docScroll && !booIsFixed) {
								//In this case, menu nav is not fixed but should be
								return 1;

							} else if (menuTop < docScroll && booIsFixed) {
								//In this case, menu nav is already fixed as it should be
								return 2;

							} else if (menuTop > docScroll && !booIsFixed) {
								//In this case, menu nav IS NOT fixed and should not be
								return 2;

							} else if (menuTop > docScroll && booIsFixed) {
								//In this case, menu nav IS fixed but should not be
								return 3;
							} else {
								return 2;
							}
						}

						$(window).scroll(function() {
							FixMenuNav();
						});

						//Click handler for menu navigation elements
						$('.menu-nav-button').click(function() {
							targ = $(this).data('target');
							$targ = $('#'+targ);
//							$targ.scrollTop(300);
							$('html, body').animate({
								scrollTop: $targ.offset().top-40
							}, 1000);

						});

					});
				</script>		
				<div id='menu-buttons'>
					<ul>
				<?php $terms = get_terms('types'); 

				foreach ($terms as $term) { ?>
					<li id='<?php echo ($term->slug . "-nav"); ?>' data-target="<?php echo ($term->slug) ;?>" class='menu-nav-button'><?php echo (substr($term->name, 2)); ?>
					</li> <!-- menu-nav-button -->
				<?php } ?>
					</ul>
				</div>
			</div> <!-- menu-nav -->

			<div id='list-menu'>

				<?php
					$taxonomies = get_object_taxonomies('food');
					$count = 0;
					foreach ($taxonomies as $tax) {
						$terms = get_terms($tax);
						foreach ($terms as $term) { ?>

							<div id='<?php echo ($term->slug); ?>' class='type'>
								<div class='type-header'>
									<div class='type-title'>
										<?php echo (substr($term->name, 2)); ?>
									</div> <!-- title -->
									<div class='type-description'>
										<?php echo( $term->description ); ?>
									</div> <!-- type-description -->
								</div> <!-- type-header -->

							<?php $foodQuery = get_food($term->name, $tax);
							while ( $foodQuery->have_posts() ) : $foodQuery->the_post(); 
								$foodID = 'food'.$post->ID; ?>

								<div id='<?php echo ($foodID); ?>' class='food-entry'>

									<div class='post-thumbnail'>
										<?php if ( has_post_thumbnail() ) {
											$thumbnail_url = wp_get_attachment_url( get_post_thumbnail_id() );

											$attachment = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');

//											echo $attachment[1];

											$size = 'thumbnail';
											$postName = the_title(""," image", false);

//											$fullSize = getimagesize($thumbnail_url);
//												echo $fullSize[0];
											the_post_thumbnail($size, array(
												'class' => 'attatchment-'.$size.' smart-image',
												'alt' => $postName,												
											    'data-fullurl' => $thumbnail_url,
												'data-fullwidth'=>$attachment[1],
												'data-fullheight'=>$attachment[2]));
										} else { ?>
											<img src="<?php echo templatePath(); ?>/images/logo.png">
										<?php } ?>
									</div>

									<div class='text'>

										<div class='title'>
											<a href='<?php the_permalink(); ?>'>
												<?php the_title(); ?>
											</a>
										</div> <!-- title -->

										<div class='the-excerpt'>
											<?php the_excerpt(); ?>
										</div> <!-- the-excerpt -->

										<div class='all-meta'>
										<?php 
										$metadata = get_post_meta(get_the_ID());
										foreach ($metadata as $key=>$val) {
											if (!(substr($key, 0, 1) == '_')) :
												foreach ($val as $subval) {
													echo ($subval . '<br/>' . "\t");
												}
											endif;
										} ?>
										</div> <!-- all-meta -->
									</div> <!-- text -->

								</div> <!-- food-entry -->
								<div class='push'> </div>
							<?php endwhile; ?>
							</div> <!-- type -->
						<?php } ?>
					<?php } ?>
			</div> <!-- list-menu -->
		<div class='push'> </div>
		</div> <!-- main -->
	</div> <!-- content -->
<?php get_footer(); ?>