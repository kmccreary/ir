<?php
function special_offer_post() {
    $labels = array (
        'name' 			=> _x('Specials', 'post type general name'),
        'singular_name' => _x('Special', 'post type singular name'),
        'add_new'		=> _x('Add New', 'specialoffer'),
        'add_new_item' 	=> __('Add New Special'),
        'edit_item' 	=> __('Edit Special' ),
        'new_item' 		=> __('New Special'),
        'all_items' 	=> __('All Specials'),
        'view_item' 	=> __('View Special'),
        'search_items' 	=> __('Search Special'),
        'not_found' 	=> __('No Special Found'),
        'not_found_in_trash' => __('No Specials Found In The Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Special Offers'
    );

    $args = array (
        'labels' => $labels,
        'description' => 'Allows creation of specials',
        'public' => true,
        'menu_position' => 4,
        'supports' => array ('title', 'editor', 'custom-fields', 'post-formats', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive' => true,
        'menu_icon' => 'dashicons-megaphone');
    register_post_type( 'specialoffer', $args );
    flush_rewrite_rules();
}

add_action( 'init', 'special_offer_post' );



function get_special_offer() {
    $args = array(
        'post_type' => 'specialoffer'
    );
    return new WP_Query( $args );
}


?>