<?php
function store_element_post() {
    $labels = array (
        'name' 			=> _x('Store Elements', 'post type general name'),
        'singular_name' => _x('Store Element', 'post type singular name'),
        'add_new'		=> _x('Add New', 'storeelement'),
        'add_new_item' 	=> __('Add New Store Element'),
        'edit_item' 	=> __('Edit Store Element' ),
        'new_item' 		=> __('New Store Element'),
        'all_items' 	=> __('All Store Elements'),
        'view_item' 	=> __('View Store Element'),
        'search_items' 	=> __('Search Store Element'),
        'not_found' 	=> __('No Store Element Found'),
        'not_found_in_trash' => __('No Store Elements Found In The Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Store Elements'
    );

    $args = array (
        'labels' => $labels,
        'description' => 'Holds our Locations and Location data',
        'public' => true,
        'menu_position' => 5,
        'supports' => array ('title', 'editor', 'custom-fields', 'post-formats', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive' => true,
        'menu_icon' => 'dashicons-store');
    register_post_type( 'storeelement', $args );
    flush_rewrite_rules();
}

add_action( 'init', 'store_element_post' );

function store_element_taxonomies() {

    //Location Geo
    $labels = array (
        'name'              => _x( 'Location Geos', 'taxonomy general name' ),
        'singular_name'     => _x( 'Location Geo', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Location Geo' ),
        'all_items'         => __( 'All Location Geos' ),
        'parent_item'       => __( 'Parent Location Geo' ),
        'parent_item_colon' => __( 'Parent Location Geo:' ),
        'edit_item'         => __( 'Edit Location Geo' ),
        'update_item'       => __( 'Update Location Geo' ),
        'add_new_item'      => __( 'Add New Location Geo' ),
        'new_item_name'     => __( 'New Store Element Location Geo' ),
        'menu_name'         => __( 'Location Geos' )
    );
    $args = array(
        'labels'			=> $labels,
        'hierarchical' 		=> true
    );

    register_taxonomy ( 'locgeos', 'storeelement', $args);


    //Location Attributes
    $labels = array (
        'name'              => _x( 'Loc Attributes', 'taxonomy general name' ),
        'singular_name'     => _x( 'Loc Attribute', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Loc Attributes' ),
        'all_items'         => __( 'All Loc Attributes' ),
        'parent_item'       => __( 'Parent Loc Attribute' ),
        'parent_item_colon' => __( 'Parent Loc Attribute:' ),
        'edit_item'         => __( 'Edit Loc Attribute' ),
        'update_item'       => __( 'Update Loc Attribute' ),
        'add_new_item'      => __( 'Add New Loc Attribute' ),
        'new_item_name'     => __( 'New Store Element Loc Attribute' ),
        'menu_name'         => __( 'Loc Attributes' )
    );
    $args = array(
        'labels'			=> $labels,
        'hierarchical' 		=> true
    );

    register_taxonomy ( 'locattributes', 'storeelement', $args);
}

add_action( 'init', 'store_element_taxonomies', 0 );

function get_store_element($location,$attribute) {
    $args = array(
        'post_type' => 'storeelement',
        'tax_query' => array(
            array(
                'taxonomy' => 'locgeos',
                'field' => 'slug',
                'terms' => $location
            ),
            array(
                'taxonomy' => 'locattributes',
                'field' => 'slug',
                'terms' => $attribute
            )
        )
    );
    return new WP_Query( $args );
}


?>