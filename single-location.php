<?php get_header(); ?>
<!--            -->
<!-- Single-location.php -->
<!--            -->

	<div id='content'>
		<div id='main'>
		<?php if ( have_posts() ) : the_post() ?>
			<div class='title'>
				<?php the_title(); ?>
			</div> <!-- title -->
			<div class='text'>
				<?php the_content() ?>
			</div> <!-- text -->
			<div id='map' onload='initMap()'>
			</div> <!-- map -->
			<script type="text/javascript">
				var map;
				var geocoder;
				function initMap() {
					map = new google.maps.Map(document.getElementById('map'), {
						zoom: 12,
						center: {lat: -34.397, lng: 150.644}
					});
					geocoder = new google.maps.Geocoder();
					geocodeAddress(geocoder, map);
				}

				function geocodeAddress(geocoder, resultsMap) {
					var address = "<?php echo ( get_post_meta(get_the_ID(), 'address', true) ); ?>";
					geocoder.geocode({'address' : address}, function (results, status) {
						if (status === google.maps.GeocoderStatus.OK) {
							resultsMap.setCenter(results[0].geometry.location);
							var marker = new google.maps.Marker({
								map: resultsMap,
								position: results[0].geometry.location
							});
						} else {
							alert('Geocode was not successful for the following reason: ' + status);
						}
					});
				}
				initMap();
			</script>
		<?php endif ?>	
		</div> <!-- main -->
	</div> <!-- content -->

<?php get_footer();?>