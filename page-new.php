<?php include('header.php');?>
    <section id="header">
        <div id="header_graphic_container">

        </div>
        <div id="header_text_container">
            <h1>Mexican Food So Fresh</h1><h2 id="blush">You'll Blush!</h2>

            <h3 id="fresh_is_great">Great Mexican food is great only if you use the freshest of fresh ingredients. And that's exactly what we do at Iguanas Ranas.</h3>
        </div>
        <div id="header_location_container">
            <ul>
                <li class="location"><a href="#">Texas City</a></li>
                <li class="location_separator"><img src="<?php echo $templatePath ?>/images/diamond.png"></li>
                <li class="location"><a href="#">Pearland</a></li>
                <li class="location_separator"><img src="<?php echo $templatePath ?>/images/diamond.png"></li>
                <li class="location"><a href="#">Seabrook</a></li>
            </ul>
        </div>
        <div class="push">&nbsp;</div>
    </section>









<?php include('footer.php');?>