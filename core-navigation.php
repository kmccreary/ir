<nav id="nav_menu">
    <a href="<?php bloginfo('url');?>"><img src="<?php echo templatePath(); ?>/images/logo.png"></a>
    <ul>
        <li><a href="<?php bloginfo('url');?>">Home</a></li>
        <li><a href="<?php bloginfo('url');?>/menu">Menu</a></li>
        <li><a href="<?php bloginfo('url');?>/catering">Catering</a></li>
        <li id="location_menu"><a href="#"><span class="the_word">Locations</span></a></li>
        <li><a href="<?php bloginfo('url');?>/about">About</a></li>
        <li><a href="<?php bloginfo('url');?>/contact">Contact</a></li>

    </ul>
</nav>
<script>
    $(document).ready(function(){

        //move the sub nav section to the body tag
//        $('body').append($('#sub_nav'));

        //Create div and append to body for semi-opaque overlay
        overlay = "<div id='overlay'></div>";
        $('body').append(overlay);

        //Set the properties of the overlay but make it hidden
        $over = $('#overlay');
        $over.css({
            'position' : 'fixed',
            'top':'0px',
            'width':'100%',
            'height':'100%',
            'background-color':'#555555',
            'opacity':'.1',
            'display':'none'
        });

        //Attach event handler to the overlay
        $over.click(function() {
            $('#sub_nav').slideUp(200);
            $over.hide();
        });

        // attach event handler to menu items
        $('#location_menu').click(function(e) {
            //work with overlay

            //Get coordinates of enclosed span

            offset = $('#location_menu span').offset();
            targId = $('#sub_nav');

            if($(targId).is(':visible')) {
                $('#sub_nav').hide();
                $over.hide();
            } else {

                //Set attributes on target
                $over.show();
                $(targId).css({
                    'top':$('#combo').height()+2,
                    'left':offset.left,
//                    'opacity':'.1',
                    'display':''
                }).slideDown(200);
            }
        });

        $(window).resize(function() {
            $('#sub_nav').slideUp(200);
            $over.hide();
        }).scroll(function() {
            $('#sub_nav').slideUp(200);
            $over.hide();
        });

    });
</script>
<div id="sub_nav">
    <ul>
        <a href="<?php bloginfo('url');?>/texas-city"><li id="texas_city_location">
            <div class="outer">
                <div class="graphic">
                    Texas<br>City
                </div>
                <div class="sub_nav_text_container">
                    <h1>The Original</h1>
                    <div class="sub_nav_body_text">A perfect place to kick back and relax with a draft beer or tasty margarita.</div>
                </div>
                <div class="push"></div>
            </div>
        </li></a>
        <a href="<?php bloginfo('url');?>/pearland"><li id="pearland_location">
            <div class="outer">
                <div class="graphic">
                    Pearland
                </div>
                <div class="sub_nav_text_container">
                    <h1>Numero Dos</h1>
                    <div class="sub_nav_body_text">The ultimate Tex-Mex sports bar and family hang out. 20 TVs plus a game room.</div>
                </div>
                <div class="push"></div>
            </div>
        </li></a>
        <a href="<?php bloginfo('url');?>/seabrook"><li id="seabrook_location">
            <div class="outer">
                <div class="graphic">
                    Seabrook
                </div>
                <div class="sub_nav_text_container">
                    <h1>By The Bay</h1>
                    <div class="sub_nav_body_text">Enjoy the bay breezes while you relax on our outdoor patio.</div>
                </div>
                <div class="push"></div>
            </div>
        </li></a>
    </ul>
</div>

<div id="mobileNav">
    <a id="left-menu" href="#left-menu"><img src="<?php echo templatePath(); ?>/images/mobilenav.png"></a></div>

<div id="sidr">
    <ul>
        <li><a href="<?php bloginfo('url');?>">Home</a></li>
        <li><a href="<?php bloginfo('url');?>/menu">Menu</a></li>
        <li><a href="<?php bloginfo('url');?>/catering">Catering</a></li>
        <li><a>Locations</a>
            <ul>
                <li><a href="<?php bloginfo('url');?>/texas-city">Texas City</a></li>
                <li><a href="<?php bloginfo('url');?>/pearland">Pearland</a></li>
                <li><a href="<?php bloginfo('url');?>/seabrook">Seabrook</a></li>
            </ul>
        </li>
        <li><a href="<?php bloginfo('url');?>/about">About</a></li>
        <li><a href="<?php bloginfo('url');?>/contact">Contact</a></li>
    </ul>
</div>
