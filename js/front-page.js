/**
 * Created by kevinmccreary on 10/29/15.
 */
$(document).ready(function() {

    ////Call resize when doc is ready
    //resizeCombo();
    //
    ////Resizes the overlay for navigation.
    //function resizeCombo() {
    //    $('#combo').height($('nav').height());
    //}

    //dynamically set font size based on window width.
    function setFontSizeBlush() {
        fontSize = 92;
        if($(window).width() > 897 && $(window).width() < 1248 ) {
            fontSize=64;
        } else if ($(window).width() <= 897){
            fontSize=52;
        }
        return fontSize;

    }

    //Animate elements if window width is greater than 618 px
    if ($(window).width() > 618) {
        $('#blush').delay(1000).fadeIn().animate({'font-size':setFontSizeBlush()});
        $('#fresh_is_great').delay(2000).fadeIn(200);
    } else {
        $('#blush').css({
            'font-size':setFontSizeBlush(),
            'display':'inherit'
        });
        $('#fresh_is_great').css('display','inherit');
    }


    //Set font size upon window resize event.
    $(window).resize(function() {
        newSize = setFontSizeBlush() + 'px';
        $('#blush').css({'font-size':newSize});
        //resizeCombo();
    });


    //grab the images from the img gallery and place in graphic container
    $('#header_graphic_container').html($('#kjm_img_gallery').html());

    //Set timer to call rotateImage()
    window.setInterval(rotateImage, 4000);

    //function that rotates main image
    function rotateImage() {
        //select the last item
        $last = $('#header_graphic_container img:last-child');

        //clone the last item
        $lastClone = $last.clone();

        //Wrap the clone in a div
        $lastClone.wrap('<div>');

        //Get the html of the clone
        var html = $lastClone.parent().html();

        //Fade out the last image
        $last.fadeOut(1500);
//
        //User timer to move last item to before first item.
        window.setTimeout(function() {
            $('#header_graphic_container img:first-child').before(html);
            $last.remove();
        },1500);
    }

});