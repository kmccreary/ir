<?php
function menu_item_post() {
    $labels = array (
        'name' 			=> _x('Foods', 'post type general name'),
        'singular_name' => _x('Food', 'post type singular name'),
        'add_new'		=> _x('Add New', 'book'),
        'add_new_item' 	=> __('Add New Food'),
        'edit_item' 	=> __('Edit Food' ),
        'new_item' 		=> __('New Food'),
        'all_items' 	=> __('All Foods'),
        'view_item' 	=> __('View Food'),
        'search_items' 	=> __('Search Foods'),
        'not_found' 	=> __('No Foods Found'),
        'not_found_in_trash' => __('No Foods Found In The Trash'),
        'parent_item_colon' => '',
        'menu_name' => 'Foods'
    );

    $args = array (
        'labels' => $labels,
        'description' => 'Holds our Foods and Food data',
        'public' => true,
        'menu_position' => 5,
        'supports' => array ('title', 'editor', 'custom-fields', 'post-formats', 'thumbnail', 'excerpt', 'comments' ),
        'has_archive' => true,
        'menu_icon' => 'dashicons-carrot');
    register_post_type( 'food', $args );
    flush_rewrite_rules();
}

add_action( 'init', 'menu_item_post' );

function menu_item_taxonomies() {

    //Meals Taxonomy
    // $labels = array (
    //     'name'              => _x( 'Meals', 'taxonomy general name' ),
    //     'singular_name'     => _x( 'Meal', 'taxonomy singular name' ),
    //     'search_items'      => __( 'Search Meals' ),
    //     'all_items'         => __( 'All Meals' ),
    //     'parent_item'       => __( 'Parent Meal' ),
    //     'parent_item_colon' => __( 'Parent Meal:' ),
    //     'edit_item'         => __( 'Edit Meal' ),
    //     'update_item'       => __( 'Update Meal' ),
    //     'add_new_item'      => __( 'Add New Meal' ),
    //     'new_item_name'     => __( 'New Food Meal' ),
    //     'menu_name'         => __( 'Meals' )
    // );

    // $args = array(
    //     'labels'			=> $labels,
    //     'hierarchical' 		=> true
    // );

    // register_taxonomy ( 'meals', 'food', $args);

    //Type Taxonomy
    $labels = array (
        'name'              => _x( 'Types', 'taxonomy general name' ),
        'singular_name'     => _x( 'Type', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Types' ),
        'all_items'         => __( 'All Types' ),
        'parent_item'       => __( 'Parent Type' ),
        'parent_item_colon' => __( 'Parent Type:' ),
        'edit_item'         => __( 'Edit Type' ),
        'update_item'       => __( 'Update Type' ),
        'add_new_item'      => __( 'Add New Type' ),
        'new_item_name'     => __( 'New Food Type' ),
        'menu_name'         => __( 'Types' )
    );

    $args = array(
        'labels'			=> $labels,
        'hierarchical' 		=> true
    );

    register_taxonomy ( 'types', 'food', $args);

    // $labels = array (
    //     'name'              => _x( 'Attributes', 'taxonomy general name' ),
    //     'singular_name'     => _x( 'Attribute', 'taxonomy singular name' ),
    //     'search_items'      => __( 'Search Attributes' ),
    //     'all_items'         => __( 'All Attributes' ),
    //     'parent_item'       => __( 'Parent Attribute' ),
    //     'parent_item_colon' => __( 'Parent Attribute:' ),
    //     'edit_item'         => __( 'Edit Attribute' ),
    //     'update_item'       => __( 'Update Attribute' ),
    //     'add_new_item'      => __( 'Add New Attribute' ),
    //     'new_item_name'     => __( 'New Food Attribute' ),
    //     'menu_name'         => __( 'Attributes' )
    // );

    // $args = array(
    //     'labels'			=> $labels,
    //     'hierarchical' 		=> false
    // );

    // register_taxonomy ( 'attributes', 'food', $args);


}

add_action( 'init', 'menu_item_taxonomies', 0 );

function get_food($term, $tax = 'types') {
    $args = array(
        'post_type' 	=> 'food',
        'tax_query' 	=> array( array(
            'taxonomy'	=> $tax,
            'field'		=> 'name',
            'terms'		=> $term)
        )
    );
    return new WP_Query( $args );
}
?>