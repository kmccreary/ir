<?php get_header(); ?>
    <!--          -->
    <!-- Page.php -->
    <!--          -->

    <div id='content'>
        <div id='main'>
            <?php if ( have_posts() ) : the_post() ?>
                <div class='back-page-title'>
                    <?php the_title() ?>
                </div> <!-- title -->
                <div id="back-page-text" class='text'>
                    <?php the_content() ?>
                    <div class="push"></div>
                </div> <!-- text -->
            <?php endif ?>


            <?php $query = new WP_Query( array( 'category_name' => 'catering','orderby'=>'title', 'order'=>'asc') ); ?>
            <?php if($query->have_posts()) : ?>
                <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                    <div class="catering_food_category">
                    <div class="title_bar"><?php echo substr(get_the_title(),-strlen(get_the_title())+3); ?></div>
                    <div class="food_description"><?php the_content(); ?></div>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>
            <br/>
            <p>Orders for 20 persons or more receive free sopapillas!</p>
            <br/>
            <p>Sweet or Unsweetened tea $10.99/gallon</p>
            <p>Fresh lemonade $12.99/gallon</p>
            <p>Includes ice, cups, straws and sweeteners.</p>

<script>
    $(document).ready(function() {

        setCateringBoxHeight();

        function setCateringBoxHeight() {
            var $boxes = $(".obtain_type");
            var maxHeight = 0
            for (var i = 0;i<$boxes.length;i++) {
                var item = $($boxes[i]);
                maxHeight = item.height() > maxHeight ? item.height() : maxHeight;
            }
            $boxes.height(maxHeight);
        }

        $(window).resize(function() {
            //remove style so that it can auto resize the height
            $(".obtain_type").css("height","");
            // call function to set the height
            setCateringBoxHeight();
        });

    });
</script>
            <section id="obtain_types">
                <div id="fullservice_container" class="obtain_type">
                    <div class="title_bar">Full Service</div>
                    <img class="catering_image" src="<?php echo templatePath(); ?>/images/catering_full_service.png" >
                    <div class="obtain_description">
                        <p>We will provide buffet tables with decor and food service, chafing dishes, plates, cutlery,
                            cups, etc.</p>
                    </div>
                </div>
                <div id="pickup_container" class="obtain_type">
                    <div class="title_bar">Pickup</div>
                    <img class="catering_image" src="<?php echo templatePath(); ?>/images/catering_pickup.png" >
                    <div class="obtain_description">
                        <p>Food can be packed individually or in bulk. Includes serving utensils, plates and cutlery.</p>
                    </div><div class="push"></div>
                </div>
                <div id="delivery_container" class="obtain_type">
                    <div class="title_bar">Delivery</div>
                    <img class="catering_image" src="<?php echo templatePath(); ?>/images/catering_delivery.png" >
                    <div class="obtain_description">
                        <p>Food can be packed individually or in bulk.Includes serving utensils, plates and cutlery. $35 Delivery Fee</p>
                    </div>
                </div>
            </section>
            <div class="push"></div>

            <br/>
            <p>Charge 15% service fee, or minimum of $100 and a $35 delivery fee.</p>
            <br/>
            <p>All major credit cards accepted.</p>
        </div> <!-- main -->
    </div> <!-- content -->


<?php get_footer(); ?>