<?php get_header();?>
    <section id="header">
        <div id="header_graphic_container">
        
        </div>
        <div id="header_text_container">
            <h1>Mexican Food So Fresh</h1><h2 id="blush">You'll Blush!</h2>
            
            <h3 id="fresh_is_great">Great Mexican food is great only if you use the freshest of fresh ingredients. And that's exactly what we do at Iguanas Ranas.</h3>
        </div>
        <div id="header_location_container">
            <ul>
                <li class="location"><a href="<?php bloginfo('url'); ?>/texas-city">Texas City</a></li>
                <li class="location_separator"><img src="<?php echo templatePath(); ?>/images/diamond.png"></li>
                <li class="location"><a href="<?php bloginfo('url'); ?>/pearland">Pearland</a></li>
                <li class="location_separator"><img src="<?php echo templatePath(); ?>/images/diamond.png"></li>
                <li class="location"><a href="<?php bloginfo('url'); ?>/seabrook">Seabrook</a></li>
            </ul>
        </div>
        <div class="push">&nbsp;</div>
    </section>


    <div id="kjm_img_gallery">
        <img src="<?php echo templatePath(); ?>/images/combo_enchilada_closeup.jpg">
        <img src="<?php echo templatePath(); ?>/images/steak_enchilada_closeup.jpg">
        <img src="<?php echo templatePath(); ?>/images/gallery_07.jpg">
        <img src="<?php echo templatePath(); ?>/images/gallery_08.jpg">
        <img src="<?php echo templatePath(); ?>/images/gallery_09.jpg">
    </div>
<div id="sec01" class="parallax-container" data-parallax="scroll" data-position="top" data-bleed="10" data-image-src="<?php echo templatePath(); ?>/images/ir_background_03.jpg" data-natural-width="1766" data-natural-height="1766">

    
    <div class="outerband" >
        <div class="innerband">
            <div class="content_outer_box">
                <div class="graphic">
                    <img src="<?php echo templatePath(); ?>/images/iguana_salad.jpg">
                </div>
                <div class="text_container">
                    <h1>FRESH FOOD</h1>
                </div>
                
            </div>
            <div class="content_outer_box">
                <div class="graphic">
                    <img src="<?php echo templatePath(); ?>/images/waitress_closeup.jpg">
                </div>
                <div class="text_container">
                    <h1>FRESH FACES</h1>
                </div>
                
            </div>
            <div class="content_outer_box">
                <div class="graphic">
                    <img src="<?php echo templatePath(); ?>/images/pdr_seabrook.jpg">
                </div>
                <div class="text_container">
                    <h1>FRESH ATMOSPHERE</h1>
                </div>
                
            </div>
            <div class="content_outer_box">
                <div class="graphic">
                    <img src="<?php echo templatePath(); ?>/images/bartender.jpg">
                </div>
                <div class="text_container">
                    <h1>FRESH DRINKS</h1>
                </div>
                
            </div>
        </div>
    </div>
    
        <div class="push"></div>
    </div>

    

    <section id="sec02" class="one outerband" >
        
<script>
    $(document).ready(function() {
        
        //Harness to call function that sets vertical position of text container.
        function callFunction() {
            $txt = $('#sec02 .text_container');
            $cnt = $('#sec02');
            if($(window).width() > 557) {
                setTextVertPosition($txt, $cnt);
            }else {
                $txt.removeAttr('style');
            }
        }
        
        callFunction();
        
        //Set margin on text container so that it is vertically centered.
        function setTextVertPosition($textObject, $container) {
            newMarg = ($container.height() - $textObject.height())/2;
            $textObject.css('margin-top',newMarg);
        }
        
        $(window).resize( function() {
            callFunction();
        });
        
    });
        
</script>        
        
        
        <div class="innerband">
            <div class="graphic_container">
                <div class="graphic">
                    <img src="<?php echo templatePath(); ?>/images/brothers_fore.jpg">
                </div>
            </div>
            <div class="text_container">
                <h1>Two Cousins and a Dream</h1>
                <p>Javier and Sergio came to the USA wanting to make a difference for themselves and their families. After many years working for other restaurants, they realized they could not only further their dream but give back to the community by opening Iguanas Ranas. So they took a leap of faith.</p>
            </div>
        </div>
        <div class="push"></div>
    </section>


    
    
    <div class="parallax-container" data-parallax="scroll" data-position="top" data-bleed="10" data-image-src="<?php echo templatePath(); ?>/images/bar_scene.jpg" data-natural-width="1000" data-natural-height="668">
    
    
    
    </div>
    
    
    <section id="sec03" class="one" >
<script>
    $(document).ready(function() {
        
        //Harness to call function that sets vertical position of text container.
        function callFunction() {
            $txt = $('#sec03 .text_container');
            $cnt = $('#sec03');
            if($(window).width() > 557) {
                setTextVertPosition($txt, $cnt);
            }else {
                $txt.removeAttr('style');
            }
        }
        
        callFunction();
        
        //Sets top margin so as to position in middle horizontally. Takes into account
        //that parent may have padding.
        function setTextVertPosition($textObject, $container) {
            newMarg = ($container.height() - $textObject.height())/2;
            //The innerband may have some padding so need to take that into account
            pad = $('#sec03 .innerband').css('padding-top').replace("px","");
            newMarg -= pad;
            
            $textObject.css('margin-top',newMarg);
        }
        
        $(window).resize( function() {
            callFunction();
        });
        
    });
        
</script>        
        <div class="innerband">
            <div class="graphic_container">
                <div class="graphic">
                    <img src="<?php echo templatePath(); ?>/images/fresh_ingredients_fore.jpg">
                </div>
            </div>
            <div class="text_container">
                <img id="quote_open" class="quote" src="<?php echo templatePath(); ?>/images/quote_open.png"><h1>Our passion for cooking with fresh ingredients means every guest is served the best possible meal.</h1><img id="quote_close"  class="quote" src="<?php echo templatePath(); ?>/images/quote_close.png">
                <p>
                </p>
            </div>
        <div class="push"></div>
        </div>
        <div class="push"></div>
    </section>

    
    
    <div class="parallax-container" data-parallax="scroll" data-position="top" data-bleed="10" data-image-src="<?php echo templatePath(); ?>/images/ir_background.jpg" data-natural-width="1500" data-natural-height="2017"></div>
    
    
    <section id = "sec04" class="outerband">
        <div class="innerband">
            <div id="locations_container">
                <a href="<?php bloginfo('url'); ?>/texas-city" class="location_holder">
                    Texas City
                </a>
                <a href="<?php bloginfo('url'); ?>/pearland" class="location_holder">
                    Pearland
                </a>
                <a href="<?php bloginfo('url'); ?>/seabrook" class="location_holder">
                    Seabrook
                </a>
            </div>
        </div>
    </section>
    
    
<div class="parallax-container" data-parallax="scroll" data-position="top" data-bleed="10" data-image-src="<?php echo templatePath(); ?>/images/beer_and_chips.jpg" data-natural-width="1499" data-natural-height="1000"></div>

<?php include('footer.php');?>