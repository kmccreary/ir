<?php get_header(); ?>
	<div id='content'>
		<div id='gallery-menu'>

			<div id='term-buttons'>
				<?php
				$taxonomies = get_object_taxonomies('food');
				foreach ($taxonomies as $tax) {
					$terms = get_terms($tax, 'hide_empty=0');
					foreach ($terms as $term) { 
						$divID = $term->slug; ?>
					<div id='<?php echo ($divID); ?>' class='term-button'>
						<?php echo ($term->name); ?>
						<script> 
							$('#<?php echo ($divID); ?>').click(function(){
								$('.food-button').hide();
								$('.<?php echo ($divID); ?>').toggle(500).css("display", "inline-block");
							});
						</script>
					</div> <!-- term-button -->
				<?php
					}
				} ?>

			</div> <!-- term-buttons -->

			<div id='food-buttons'>
				<?php
				$foods = new WP_Query('post_type=food');
				while ($foods->have_posts() ) : $foods->the_post(); 
					
					 ?>
				<div class='food-button<?php echo_current_post_terms($post->ID) ?>'>
					<a href='<?php the_permalink() ?>'>
						<span class='spanlink'> </span>
					</a>					
					<div class='food-button-title'>
						<?php echo ($post->post_title); ?>
					</div> <!-- food-button-title -->

					<div class='food-button-image'>
						<?php if ( has_post_thumbnail() ) : the_post_thumbnail(); endif;?>
					</div> <!-- food-button-image -->

				</div> <!-- food-button -->
				<?php endwhile; ?>
			</div> <!-- food-buttons -->

		</div> <!-- gallery-menu -->
	</div> <!-- content -->
<?php get_footer(); ?>