<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="description" content="Iguanas Ranas">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1"/>

	<link rel="icon" type="image/png" href="<?php echo templatePath(); ?>/images/favicon_small.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo templatePath(); ?>/images/favicon_medium.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo templatePath(); ?>/images/favicon_large.png" sizes="96x96">
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/reset.css">
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/core-navigation.css">
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/mobilenav.css">
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/footer.css">
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/special-offers.css">
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/jquery.sidr.dark.css">
	<script src="<?php echo templatePath(); ?>/js/jquery-2.1.4.min.js"></script>
	<script src="<?php echo templatePath(); ?>/js/jquery.sidr.min.js"></script>
	<script src="<?php echo templatePath(); ?>/js/smart-image.js"></script>
	<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/smart-image.css">
	<link href='https://fonts.googleapis.com/css?family=Bitter:400,700|Open+Sans:300|Indie+Flower|Crafty+Girls' rel='stylesheet' type='text/css'>

	<script>
		$(document).ready(function() {
			//Call resize when doc is ready
			resizeCombo();

			//Resizes the overlay for navigation.
			function resizeCombo() {
				$('#combo').height($('nav').height());
			}

			$(window).resize(function() {
				resizeCombo();
			});
			$('#left-menu').sidr({
				side:'right'
			});

		});
	</script>

	<?php
	//This fetches global variable if present on page. If not, sets it to "default".
	//Used below to pull the style sheet specific to locations.
	global $tpl_body_id;
	if (!$tpl_body_id) {
		$tpl_body_id = 'default';
	}
	?>

	<?php if ( is_front_page() ) {?>
		<script src="<?php echo templatePath(); ?>/js/front-page.js"></script>
		<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/front-page.css">
		<title>Iguanas Ranas</title>
	<?php }?>
	<?php if ( !is_front_page() && $tpl_body_id == 'default') {?>
		<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/pages.css">
	<?php }?>

	<?php if ( !is_front_page() && $tpl_body_id == 'location') {?>
		<link rel='stylesheet' type="text/css" href="<?php echo templatePath(); ?>/css/location.css">
		<script src="https://maps.googleapis.com/maps/api/js"></script>
	<?php }?>

	<?php if ( is_single() ) : ?>
		<title> <?php single_post_title(); ?> </title>
		<link rel='stylesheet' type='text/css' href='<?php echo templatePath(); ?>/css/comments.css'>
	<?php elseif (is_page() ) : ?>
		<title> <?php single_post_title(); ?> </title>
	<?php elseif (is_404() ) : ?> 
		<title> 404: Page Not Found ?> </title>
	<?php else : ?>
		<title> Iguanas Ranas </title>
	<?php endif; ?>


	<script src="<?php echo templatePath(); ?>/js/parallax.js"></script>
	<?php wp_head();?>
</head>
<body>
<div id = "combo" class="parallax-container" data-parallax="scroll" data-position="top" data-bleed="10" data-image-src="<?php echo templatePath(); ?>/images/ir_background_02.jpg" data-natural-width="1454" data-natural-height="680">
</div>
<?php include('special-offers.php'); ?>
