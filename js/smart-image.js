/**
 * Created by Kevin on 11/5/2015.
 */
$(document).ready(function() {
    //First, append a popup overlay to the body
    $('body').append(
        "<div id='popup_overlay'></div>"
    );

    //Set the CSS on the new element
    $('#popup_overlay').css({
        'display':'none',
        'background-color':'#555',
        'position':'fixed',
        'z-index':'3',
        'top':'0px',
        'left':'0px',
        'height':'100%',
        'width':'100%'
    }).click(function() {
        closePopup();
    });

    //build the html string for the image container that will be
    //dynamically inserted when the user clicks on an image
    dynHtml = "<div class='smart-image-container'>"
        + "<div class='smart-full-image'></div>"
        + "<div class='smart-full-image-text'></div>"
        + "<div class='smart-full-image-close'>Close</div>"
        + "</div>"
    ;
    //Handler for click on a smart image
    $('.smart-image').click(function() {
        console.log($(this).data("fullurl"));
        console.log($(this).data("fullheight"));


        //Determine if a dynamic image container already exists
        //If dynamic image container doesn't already exist after img div, insert it.
        nextItem = $(this).next().attr('class');
        if(nextItem == undefined || nextItem != 'smart-image-container') {
            $(this).after(dynHtml);
        } else {
            showFullImage($(this).next());
            return;
        }

        //Insert the image div in the container
        $container = $(this).next().find('.smart-full-image').first();   //$((this).parent() + ' .smart-full-image');
        htmlString = "<img src='" + $(this).data('fullurl') + "'/>"
        $container.html(htmlString);

        //Attach event handler to the close text button
        $(this).next().find('.smart-full-image-close').first().click(function() {
            closePopup();
        });

        showFullImage($(this).next());

    });
    function showFullImage($item) {
        //Show the full image
        $item.css('display','inherit');

        console.log($item.prev().data("fullheight"));

        //fetch the image
        $img = $item.find('.smart-full-image img');

        //Display the overlay so the main page is obscured
        $('#popup_overlay').show().css({
            'opacity':'.8'
        });

        // Get the width of the window
        winWidth = $(window).width();

        // Get the height of the window
        winHeight = $(window).height();

        // Get the width of the full image
        imgWidth = $item.prev().data("fullwidth");//$img.width();

        // Get the height of the full image
        imgHeight = $item.prev().data("fullheight");//$img.height();
//                        console.log(
//                            winWidth + " | " +
//                            winHeight + " | " +
//                            imgWidth + " | " +
//                            imgHeight
//                        );

        //Set initial container width to zero.
        containerWidth = 0;
        containerHeight = 0;
        //Run through set of possible image dimensions compared to window dimensions


        if(imgHeight >= winHeight && imgWidth < winWidth) { //Image is taller and thinner than window
            console.log('1');
            containerHeight = winHeight * .8;
            containerWidth = containerHeight / imgHeight * imgWidth;

        } else if (imgWidth >= winWidth && imgHeight < winHeight) { //Image is shorter and wider than window
            console.log('2');
            containerWidth = winWidth * .8;
            containerHeight = containerWidth / imgWidth * imgHeight;


        } else if (imgWidth >= winWidth && imgHeight >= winHeight) { //Image is taller and wider than window
            console.log('3');

            //Since width and height of image are greater than the window, need to determine which dimension to use
            //for sizing the image.
            widFactor = imgWidth / winWidth;
            heiFactor = imgHeight / winHeight;
            console.log('wf: ' + widFactor + " | hf: " + heiFactor);

            if(widFactor > heiFactor) {
                containerWidth = winWidth * .8;
                containerHeight = containerWidth / imgWidth * imgHeight;
            } else {
                containerHeight = winHeight * .8;
                containerWidth = containerHeight / imgHeight * imgWidth;
            }
        } else { //Image is shorter and thinner than window
            console.log('4');
            containerWidth = imgWidth;
            containerHeight = imgHeight;
        }
        console.log(
            winWidth + " | " +
            winHeight + " | " +
            imgWidth + " | " +
            imgHeight
        );
        console.log('cw: ' + containerWidth + " | ch: " + containerHeight);

        //get width and height padding
        widPadding  = parseInt($item.css('padding-left').replace("px",""))
            + parseInt($item.css('padding-right').replace("px",""));

        heiPadding = parseInt($item.css('padding-top').replace("px",""))
            + parseInt($item.css('padding-bottom').replace("px",""));


        //Calculate positioning
        posX = (winWidth - (containerWidth + widPadding)) / 2;
        posY = (winHeight - (containerHeight+30+heiPadding))/2;

        console.log(
            'window width: ' + winWidth +
            ' | container width: ' + containerWidth +
            ' | left position: ' + posX
        );

        //Finally, set the dimensions of the popup window
        $item.height(containerHeight + 30).width(containerWidth)
            .css({
                'top':posY,
                'left':posX,
                'z-index':'10'
            });

        //Set the dimensions of the enclosed image.
        $img.css({
            'height':'100%',
            'width' : '100%'
        });

    }
    function closePopup() {
        //Close the image
        $('.smart-image-container').slideUp(250);
        $('#popup_overlay').hide();

        //After closing the popup, need to remove the styles from the container
        //Have to use timeout so that styles are not removed before slideUp is complete.
        window.setTimeout(function() {
            $('.smart-image-container').removeAttr('style');
            console.log('should have removed');
        },300);
    }

});