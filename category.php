<?php get_header(); ?>
<!--              -->
<!-- Category.php -->
<!--              -->

	<div id='archive'>
		<div id='category-page'>
			<?php $url = get_bloginfo('url');
				$categories = get_categories('hide_empty=0&exclude=1'); 
				foreach($categories as $category){
					$catlink = $category->slug;
					$catname = $category->name; ?>
				<div class='category'>
					<a href='<?php echo ($url."/category/".$catlink); ?>'>
						<span class='spanlink'></span>
					</a>
					<div class='category-title'>
						<?php echo ($catname); ?>
					</div> <!-- category-title -->
				</div> <!-- category -->
			<?php }?>
			<div class='push'></div>
			<hr>

			<?php $currentCatID = get_cat_ID(single_cat_title('', false));
			$menuItems = new WP_Query(array('post_type' => 'post', 'cat' => '-'.get_cat_ID('Coaching Services')));
			if ( $menuItems->have_posts() ) : 
			while ( $menuItems->have_posts() ) : $menuItems->the_post() ?>
			<div class='menu-item'>
				<?php if ( has_post_thumbnail() ) : the_post_thumbnail(); endif;?>
				<a href='<?php echo (the_permalink()) ?>'>
					<span class='spanlink'></span>
				</a>
				<div class='menu-item-name'>
					<?php the_title(); ?>
				</div>
			</div> <!-- menu-item -->
			<?php endwhile; endif; ?>


		</div> <!-- category-page -->
	</div> <!-- archive -->

<?php get_footer(); ?>