<?php
$myQuery = get_special_offer();

    if($myQuery->have_posts()) { ?>
        <div id="special_offer">
            <script>
                $(document).ready(function() {
                    //Displays the special offer div
                    $('#special_offer').delay(750).slideDown(750);

                    //Assign click handler to close special offer div
                    $('#close_special_offer').click(function() {
                        $('#special_offer').slideUp();
                    });

                });
            </script>

        <?php // The Loop
        while ( $myQuery->have_posts() ) : $myQuery->the_post();

            global $post;
            ?>
            <div id="special_offer_image_container">
            <?php
            //Pulls the thumbnail image without any hardcoded dimensions in the element tag
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
            $imageId = get_post_thumbnail_id( $post->ID );
            $fullImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
            $fullSize = array($fullImage[1], $fullImage[2]);
            if ($image) : ?>
                <img class="smart-image" id="special_offer_image" src="<?php echo $image[0]; ?>" alt="" data-fullurl = "<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' )[0];
                ?>" data-imageid = "<?php echo $imageId;?>" data-fullwidth = "<?php echo $fullSize[0];?>" data-fullheight = "<?php echo $fullSize[1];?>"/>
            <?php endif;?>
            </div>
            <div id="special_offer_title">
            <?php

            the_title();?>
            </div>
            <div id="special_offer_description">
            <?php
            the_content();?>
            </div>
            <?php



        endwhile; ?>
            <div class="push"></div>
            <div id="full_image_container">
                <div id="full_image"></div>
                <div id="full_image_text"></div>
                <div id="full_image_close">Close</div>
            </div>
        <div id="close_special_offer">Close</div>
        </div>
<?php
    }
// Reset Post Data
wp_reset_postdata();?>



