<?php 

include('functions_store_elements.php');
include('functions_menu_items.php');
include('functions_specials.php');

add_theme_support( 'post-thumbnails', array( 'post', 'food','storeelement','specialoffer' ) );

function echo_current_post_terms($postID) {
	$taxonomies = get_post_taxonomies( $postID );
	$all_terms = array();
	foreach ($taxonomies as $tax) {
		$tax_terms = get_the_terms( $postID, $tax );
		if (!(empty($tax_terms)) && !(is_wp_error($tax_terms))) {
			foreach ($tax_terms as $term) {
				$all_terms[] = $term->slug;
			}
		}
	}
	foreach ($all_terms as $aTerm) {
		echo ( " " . $aTerm);
	}
}
function get_upload_directory() {
	return (wp_upload_dir()['baseurl']."/");
}

function pageName() {
	return basename(get_permalink());
}

function templatePath() {
	return get_bloginfo('template_directory');
}

function get_category_id($cat_name) {
	$term = get_term_by('name',$cat_name,'category');
	return $term->term_id;
}

function get_category_id_by_slug($slug) {
	$term = get_term_by('slug',$slug,'category');
	return $term->term_id;
}


function get_child_categories($parent_id) {
	$cat_list = get_categories('child_of='.$parent_id);
	return $cat_list;
}

function get_child_categories_by_parent_slug($slug) {

	return get_categories('child_of='.get_category_id_by_slug($slug));
//        return "hello world";
}


?>