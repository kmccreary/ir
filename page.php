<?php get_header(); ?>
<!--          -->
<!-- Page.php -->
<!--          -->

	<div id='content'>
		<div id='main'>
		<?php if ( have_posts() ) : the_post() ?>
			<div class='back-page-title'>
				<?php the_title() ?>
			</div> <!-- title -->
			<div id="back-page-text" class='text'>
				<?php the_content() ?>
				<div class="push"></div>
			</div> <!-- text -->
		<?php endif ?>
		</div> <!-- main -->
	</div> <!-- content -->

	

<?php get_footer(); ?>