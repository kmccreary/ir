 <?php 

   /*********************/
  /* 	Comments.php   */
 /*********************/

 if ( post_password_required() ) {
 	return;
 } ?>

 <div id="comments">

 	<?php  if (have_comments() ) : ?>
 	<div class='comment-header'>
 		<?php
 		$theTitle = the_title('', '', false);
 		$numPosts = get_comments_number();
 		echo ( _nx( 'One comment on '.$theTitle, number_format_i18n( $numPosts)." comments on ".$theTitle, $numPosts, 'comments title'));
 		?>
 	</div> <!-- comment-header -->

 	<div class='comment-list'>
 		<?php
 			wp_list_comments( array (
 				'type' => 'comment',
 				'avatar_size' => 0,
 				'style' => 'div'
 			) );
 		?>
 	</div> <!-- comment-list -->
 	<?php endif; ?>

 	<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments') ) : ?>
 	<p class='no-comments'> Comments are closed. </p>
 <?php endif; ?>

 <?php

 $fields =  array(

  'author' =>
    '<div class="author-field"><label for="author">' . __( 'Name', 'domainreference' ) . ( $req ? '<span class="required">*</span>' : '' ) .
    '</label><p class="comment-form-author">' . '<input id="author" name="author" type="text" value="' . 
    esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p></div>',

  'email' =>
    '<div class="email-field"><label for="email">' . __( 'Email', 'domainreference' ) . ( $req ? '<span class="required">*</span>' : '' ) . 
    '</label><p class="comment-form-email">' . '<input id="email" name="email" type="text" value="' 
    . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div>',

  'url' =>
    '<div class="url-field"><label for="url">' . __( 'Website', 'domainreference' ) . '</label><p class="comment-form-url">' .
    '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
    '" size="30" /></p></div>',
);

 $commentField = '<div class="comment-textarea"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><p class="comment-form-comment">'.
 '<textarea id="comment" name="comment" aria-required="true"></textarea></p></div>';

 comment_form(array('fields'=>$fields, 'comment_field'=>$commentField)); ?>

 </div> <!-- comments -->