<?php
/*
Template Name: Store Location
*/
?>
<?php
$tpl_body_id = 'location';
?>
<?php get_header();?>
<div id="wrapper">
    <?php
    //Get the page title. The title will come from the Page itself ?>
            <?php if ( have_posts() ) : the_post() ?>
            <h1 id="location_name"><?php the_title(); ?></h1>
            <?php endif ?>




    <div id="left_column">

        <div id="store_picture_container">
            <?php //Fetch the thumbnail of the store. Intersection of Location Geo = page slug and Loc Attributes = Address.


            $geoAddress = "";
            $address = "";
            $myQuery = get_store_element(pageName(),'address');
            // The Loop
            while ( $myQuery->have_posts() ) : $myQuery->the_post();

                global $post;
                $geoAddress = get_post_meta($post->ID, 'geoaddress',true);
                $address = $post->post_content;

                //Pulls the thumbnail image without any hardcoded dimensions in the element tag
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

                if ($image) : ?>
                    <img id="store_picture" src="<?php echo $image[0]; ?>" alt="" />
                <?php endif;


            endwhile;

            // Reset Post Data
            wp_reset_postdata();?>
        </div>

        <?php //Draw the map. Address is at intersection of Location Geo = page slug and Loc Attributes = Address.?>
        <div id="map" onload='initMap()'>
        </div>
        <script type="text/javascript">
            var map;
            var geocoder;
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 14,
                    center: {lat: -34.397, lng: 150.644}
                });
                geocoder = new google.maps.Geocoder();
                geocodeAddress(geocoder, map);
            }

            function geocodeAddress(geocoder, resultsMap) {
                var address = "<?php echo $geoAddress;?>";
                geocoder.geocode({'address' : address}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location
                        });
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
            initMap();
        </script>
    </div>
    <div id="right_column">
    <div id="store_manager_container" style="display:none;">
        <?php //Fetch the manager thumbnail and name. Intersection of Location Geo = page slug and Loc Attributes = Manager
        $myQuery = get_store_element(pageName(),'manager');
        // The Loop
        while ( $myQuery->have_posts() ) : $myQuery->the_post();

            global $post;

            //Pulls the thumbnail image without any hardcoded dimensions in the element tag
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

            if ($image) : ?>
                <div id="store_manager_pic_container">
                    <img id="store_manager_pic" src="<?php echo $image[0]; ?>" alt="<?php echo $post->post_content;?>" />
                </div>
            <?php endif;?>

            <h2 id="store_manager_name">
                <?php echo $post->post_content.' - Manager';?>
            </h2>
        <?php endwhile;

        // Reset Post Data
        wp_reset_postdata();?>
    </div>


    <div id="address_container" style="display:none;">
        <?php //Show the address. Address is at intersection of Location Geo = page slug and Loc Attributes = Address.?>
        <?php echo $address;?>

        <?php //Get the phone number. Features at intersection of Location Geo = page slug and Loc Attributes = Services.
        $myQuery = get_store_element(pageName(),'phone-number');
        // The Loop
        while ( $myQuery->have_posts() ) : $myQuery->the_post();

        the_content();
        endwhile;

        // Reset Post Data
        wp_reset_postdata();?>
    </div>

    <div id="services_container">
    <?php //List the features. Features at intersection of Location Geo = page slug and Loc Attributes = Services.?>
        <h2>Services</h2>
        <?php
    $myQuery = get_store_element(pageName(),'services');
    // The Loop
    while ( $myQuery->have_posts() ) : $myQuery->the_post();
        ?><div class="location_service"><?php
        global $post;

        //Pulls the thumbnail image without any hardcoded dimensions in the element tag
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

        if ($image) : ?>
            <div class="location_service_icon">
                <img src="<?php echo $image[0]; ?>" alt="" />
            </div>
        <?php endif;?>
            <h3 class="location_service_title">
                <?php
                    the_title();?>
            </h3>
            <div class="location_service_description">
                <?php
                            the_content();?>

            </div>
        <div class="push"></div>
        </div>
            <?php
    endwhile;

    // Reset Post Data
    wp_reset_postdata();?>

    </div>

    <?php //Gallery of images
    ?>
    </div>
    <div class="push"></div>
</div>
<?php get_footer(); ?>
