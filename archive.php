<?php get_header(); ?>

	<div id='archive'>
		<div id='category-page'>
			<?php 
			$args = 'hide_empty=0';
			$terms = get_terms('menu_item_category', $args);
			foreach ($terms as $term) { ?>
				<div class='menu-post'>
					<a href='<?php echo (bloginfo('url')."/menu_item_category/".$term->slug ); ?>'>
						<span class='spanlink'> </span>
					</a>
					<div class='menu-post-name'>
						<?php echo ($term->name); ?>
					</div> <!-- menu-post-name -->
				</div> <!-- menu-post -->
			<?php } ?>
			<div class='push'> </div>
			<br>
			<hr>
			<br>

			<?php
			$currentArchive = get_query_var('menu_item_category');
			$args = array (
				'post_type' 	=> 'menu_item',
				'tax_query' 	=> array(
					array(
						'taxonomy'	=> 'menu_item_category',
						'field'		=> 'slug',
						'terms'		=> $currentArchive
					)
				)
			);
			$menuItems = new WP_Query( $args );
			while ($menuItems->have_posts() ) : $menuItems->the_post() ?>
				<div class='menu-post'>
					<a href='<?php echo ( the_permalink() ); ?>'>
						<span class='spanlink'> </span>
					</a>
					<div class='menu-post-name'>
						<?php the_title(); ?>
					</div> <!-- menu-post-name -->
				</div> <!-- menu-post -->
			<?php endwhile; ?>

		</div> <!-- category-page -->
	</div> <!-- archive -->

<?php get_footer(); ?>