<footer>
	<div id="footer_social_links">
		<ul>
			<li class="social_icon"><a href="https://www.facebook.com/Iguanas-Ranas-Cantina-Pearland-704225009658264/?fref=ts"><img src="<?php echo templatePath(); ?>/images/facebook_icon.png"></a></li>
			<li class="social_icon"><a href="https://twitter.com/iguanasranastx/"><img src="<?php echo templatePath(); ?>/images/twitter-icon.png"></a></li>
			<li class="social_icon"><a href="#"><img src="<?php echo templatePath(); ?>/images/Instagram-Icon.png"></a></li>
		</ul>
	</div>
	<div id="footer_links">
		<ul>
			<li><a href="<?php bloginfo('url'); ?>">Home</a></li>
			<li><a href="<?php bloginfo('url'); ?>/menu">Menu</a></li>
			<li><a href="<?php bloginfo('url'); ?>/catering">Catering</a></li>
			<li><a href="<?php bloginfo('url'); ?>/about">About</a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact">Contact</a></li>
		</ul>
	</div>
	<div id="footer_locations">
		<ul>
			<li><a href="<?php bloginfo('url'); ?>/texas-city">Texas City</a>
				<p>3121 Palmer Hwy</p>
				<p>Texas City, TX 77590</p>
				<p>409.995.0594</p>
			</li>
			<li><a href="<?php echo bloginfo('url'); ?>/pearland">Pearland</a>
				<p>6200 W Broadway St.</p>
				<p>Pearland, TX 77581</p>
				<p>832.230.4878</p>
			</li>
			<li><a href="<?php echo bloginfo('url'); ?>/seabrook">Seabrook</a>
				<p>2900 E Nasa Pkwy, #100</p>
				<p>Seabrook, TX 77586</p>
				<p>832.864.2855</p>
			</li>
		</ul>
	</div>
	<div id="smart_signature">
		Web Design by <a href="http://www.smartgroup-us.com">SMART Group</a>
	</div>
	<div id="copyright">
		Copyright � <?php echo date("Y");?> Iguanas Ranas
	</div>
</footer>

<?php include('core-navigation.php');?>
<?php wp_footer(); ?>
</body>
</html>