<!DOCTYPE html>
<html>
	<head>
		<?php include(get_template_directory().'/inc/helpers.php'); ?>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/reset.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/footer.css'>
		<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/header.css'>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<?php if ( is_front_page() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/pages.css'>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/front-page.css'>
			<title>Front Page Baby!</title>
		<?php elseif ( is_page() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/pages.css'> 
			<!-- Maybe not best way to implement -->
			<title><?php echo (single_post_title()) ?></title>
		<?php elseif ( is_404() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/pages.css'>
			<title>404: Content Not Found</title>
		<?php elseif ( is_archive() ) : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/blogs.css'>
			<title><?php single_cat_title('Archive: ') ?></title>
		<?php else : ?>
			<link rel='stylesheet' type='text/css' href='<?php echo $templatePath ?>/css/pages.css'>
			<title> Generic Title </title>
		<?php endif;?>
	</head>

	<body>
	<?php echo $templatePath ?>
		<div id='wrapper'>
			<div id='header'>
				Header
				<nav>
					<ul>
						<li><a href='<?php bloginfo("url") ?>'><span class='spanlink'></span></a><p>Home</p></li>
						<li><a href='<?php bloginfo("url") ?>/about-us'><span class='spanlink'></span></a><p>About</p></li>
						<li id='menu'><p>Menu</p>
							<div id='menu-dropdown'>
								<div class='menu-button'>
									<p> Gallery Menu <p>
									<a href='<?php echo ( bloginfo("url")); ?>/gallery-menu'>
										<span class='spanlink'> </span>
									</a>
								</div> <!-- menu-button -->

								<div class='menu-button'>
									<p> List Menu </p>
									<a href='<?php echo ( bloginfo("url")); ?>/list-menu'>
										<span class='spanlink'> </span>
									</a>
								</div> <!-- menu-button -->
							</div>
						</li>
						<li id='locations'><p>Locations</p>
							<div id='locations-dropdown'>
								<?php
								$locations = new WP_Query("post_type=location");
								while ($locations->have_posts() ) : $locations->the_post() ?>
								<div class='location'>
									<p><?php the_title(); ?></p>
									<a href='<?php the_permalink(); ?>'>
										<span class='spanlink'></span>
									</a>
								</div>
								<?php endwhile; ?>
							</div>
						</li>
						<li><a href='<?php bloginfo("url") ?>/contact'><span class='spanlink'></span></a><p>Contact Us</p></li>
					</ul>
				</nav>
			</div> <!-- header -->