<?php get_header(); ?>
<!--            -->
<!-- Single.php -->
<!--            -->

	<div id='content'>
		<div id='main'>
			<div id='food-post'>
			<?php if ( have_posts() ) : the_post() ?>

				<div class='title'>
					<?php the_title(); ?>
				</div> <!-- title -->

				<div class='post-thumbnail'>
					<?php if ( has_post_thumbnail() ) {
						$thumbnail_url = wp_get_attachment_url( get_post_thumbnail_id() );
						$size = 'medium';
						$postName = the_title(""," image", false);

						$fullSize = getimagesize($thumbnail_url);

						the_post_thumbnail($size, array(
							'class' => 'attatchment-'.$size.' smart-image',
							'alt' => $postName,												
						    'data-fullurl' => $thumbnail_url,
							'data-fullwidth'=>$fullSize[0],
							'data-fullheight'=>$fullSize[1]));
					} else { ?>
						<img src="<?php echo templatePath(); ?>/images/logo.png">
					<?php } ?>
				</div>

				<div class='text'>
					<?php the_content() ?>
				</div> <!-- text -->

				<div class='all-meta'>
				<?php 
				$metadata = get_post_meta(get_the_ID());
				foreach ($metadata as $key=>$val) {
					if (!(substr($key, 0, 1) == '_')) :
						foreach ($val as $subval) {
							echo ($subval . '<br/>' . "\t");
						}
					endif;
				} ?>
				</div> <!-- all-meta -->

			<?php comments_template();
			endif ?>	
			</div> <!-- food-post -->
		</div> <!-- main -->
	</div> <!-- content -->

<?php get_footer();?>